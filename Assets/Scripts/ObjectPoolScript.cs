﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Пул объектов, может быть применен для любых GameObject's
public class ObjectPoolScript : MonoBehaviour {
    public class ObjectPool
    {
        List<GameObject> list;
        string[] path;

        //Количество создаваемых объктов в пуле и путь к префабу
        public ObjectPool(int count, string[] _path)
        {
            list = new List<GameObject>();
            path = _path;

            for (int i = 0; i < count; i++)
            {
                GameObject newGameObject;
                newGameObject = Instantiate(Resources.Load<GameObject>(path[Random.Range(0, path.Length)]));
                newGameObject.SetActive(false);
                list.Add(newGameObject);
            }
        }

        //Выдача объекта из пула
        public GameObject GetObject(Vector3 v3)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].activeSelf == false)
                {
                    list[i].SetActive(true);
                    list[i].transform.position = v3;
                    return list[i];
                }
            }

            GameObject newGameObject;
            newGameObject = Instantiate(Resources.Load<GameObject>(path[Random.Range(0, path.Length)]));
            list.Add(newGameObject);
            return newGameObject;
        }
    }
}
