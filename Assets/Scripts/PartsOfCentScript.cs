﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//Части противника
public class PartsOfCentScript : MonoBehaviour {   
    
    //номер секции в противнике
    public int number;   
    //попадание
    public event System.Action<int> Hit = delegate { };
    
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bolt")
        {
            other.gameObject.SetActive(false);
            Hit(number);
            PlayerController.score += 300;
        }
    }
}
