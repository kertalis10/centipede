﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainSceneScript : MonoBehaviour
{

    //Объекты с большим количеством возможных появлений на сцене
    public static ObjectPoolScript.ObjectPool shotObjectPool;
    public static ObjectPoolScript.ObjectPool barrierObjectPool;

    //Гусеницы
    List<CentipedeScript.Centipede> centipedesList;
    int centipedeSpeed;
    //Добавляет препятствия, вызывается через [nextBomber], с интервалом [bomberRate]
    GameObject bomber;
    private float bomberRate = 5;
    private float nextBomber = 10;
    //Игрок
    GameObject player;
    //Примерные координаты области видимости камеры
    public static float Left, Right, Up, Down;
    //Массив объектов-препятствий, генерируемый до процедуры размещения этих объектов на сцене
    public static int[,] arrayOfBarriers;
    //Глобальные координаты, соответсвуют позиции [0,0] в массиве arrayOfBarriers
    public static Vector3 arrayStartPosition;
    
   
    public Text scoreText;
    public Text lifeText;  
  
    void Start()
    {
        GetCameraVisibility();

        Time.timeScale = 1;
        //Создание и размещение объекта, управляемого игроком
        player = Instantiate(Resources.Load<GameObject>("Prefabs/Shuttle"));
        player.transform.position = new Vector3(0, -4, 0);
        //Пулы выстрелов и препятствий
        shotObjectPool = new ObjectPoolScript.ObjectPool(10, new string[] { "Prefabs/LaserBlueBeam" });
        barrierObjectPool = new ObjectPoolScript.ObjectPool(200, new string[] { "Prefabs/Mushroom" });
        //Объект, добавляющий препятствия. При столкновении с игроком, игрок теряет жизнь
        bomber = Instantiate(Resources.Load<GameObject>("Prefabs/Asteroid"));
        bomber.transform.position = new Vector3(0, 6, 0);
        bomber.GetComponent<BomberScript>().Col += Lose;
        bomber.SetActive(false);
        //Создание первого противника, подписка на события
        centipedeSpeed = 2;
        CentipedeScript.Centipede centipede = new CentipedeScript.Centipede(10, centipedeSpeed);            
        centipede.Lose += Lose;
        centipede.LastHit += LastHit;
        centipede.New += New;        
        //Массив противников, в процессе игры, при делении будут добавлены новые
        centipedesList = new List<CentipedeScript.Centipede>
        {
            centipede
        };
        //Размещение препятствий на сцене
        DeployBarriers();

        PlayerController.score = 0;
        PlayerController.life = 3;
    }
    
    void FixedUpdate()
    {
        //Движение противников
        foreach(var c in centipedesList)
            c.Move();
        //Вызов объекта, добавляющего препятствия
        AddBarriers();
        
        scoreText.text = "Score: " + PlayerController.score;
        lifeText.text = "Life: " + PlayerController.life;      
    }

    //Определение границ видимости камеры
    private void GetCameraVisibility()
    {
        Up = Camera.main.orthographicSize;
        Down = -Up;
        Right = Up / Camera.main.pixelHeight * Camera.main.pixelWidth;
        Left = -Right;
    }

    //Создание массива препятствий, определение недопущения непроходимых участков, размещение объектов на сцене
    //В будущем имеет смысл перед стартом нового уровня реализовать подсчет количества оставшихся препятствий и, в случае 
    //необходимости, размещение новых
    private void DeployBarriers()
    {
        //Определение размера массива препятствий и его расположение, в зависимости от размера сцены
        int sizeArrHeight = 2 * ((int)Up * 2 - 3);
        int sizeArrWidth = 2 * ((int)Right * 2 - 6);

        arrayStartPosition = new Vector3(-sizeArrWidth / 4, Up - 1);

        arrayOfBarriers = new int[sizeArrWidth, sizeArrHeight];

        for (int i = 1; i < sizeArrWidth; i++)
            for (int j = 0; j < sizeArrHeight; j++)
            {
                if (Random.Range(0, 10) > 5)
                    arrayOfBarriers[i, j] = 1;
            }        

        //Проверка на возможные непроходимые участки     
        for (int i = 1; i < sizeArrWidth - 1; i++)
            for (int j = 1; j < sizeArrHeight; j++)
                if (arrayOfBarriers[i, j] == 1 && (arrayOfBarriers[i - 1, j - 1] == 1 || arrayOfBarriers[i + 1, j - 1] == 1))
                    arrayOfBarriers[i, j] = 0;

        //Размещение объектов на сцене
        for (int i = 0; i < sizeArrWidth; i++)
            for (int j = 0; j < sizeArrHeight; j++)
                if (arrayOfBarriers[i, j] == 1)
                {
                    barrierObjectPool.GetObject(new Vector3(-sizeArrWidth / 4 + i * 0.5f, Up - 1 - j * 0.5f, 0)).
                        GetComponent<BarrierScript>().Col += Lose; ;
                }
    }

    //При победе(уничтожении всех противников) создается новый, более быстрый
    private void Win()
    {
        if (centipedesList.Count == 0)
        {
            centipedeSpeed++;           
            CentipedeScript.Centipede centipede = new CentipedeScript.Centipede(10, centipedeSpeed);            
            centipede.Lose += Lose;
            centipede.LastHit += LastHit;
            centipede.New += New;
            centipedesList = new List<CentipedeScript.Centipede>
            {
                centipede
            };
        }
    }

    //При поражении создается новый противник, с теми-же параметрами. Игрок переносится на стартовую позицию.
    //Если у игрока не осталось доступных жизней, игровая сессия завершается
    private void Lose()
    {
        PlayerController.life--;
        if (PlayerController.life <= 0)
        {         
        }
        else
        {
            foreach (var c in centipedesList)
                c.DestoyInstance();
            
            CentipedeScript.Centipede centipede = new CentipedeScript.Centipede(10, centipedeSpeed);            
            centipede.Lose += Lose;
            centipede.LastHit += LastHit;
            centipede.New += New;
            centipedesList = new List<CentipedeScript.Centipede>
            {
                centipede
            };
            player.transform.position = new Vector3(0, -4, 0);
        }

    }

    //Уничтожение последней секции противника. 
    private void LastHit(CentipedeScript.Centipede c)
    {
        centipedesList.Remove(c);        
        Win();
    }

    //Создание нового противника при делении
    private void New(CentipedeScript.Centipede c)
    {
        CentipedeScript.Centipede centipede = new CentipedeScript.Centipede(c);
        centipede.Lose += Lose;
        centipede.LastHit += LastHit;
        centipede.New += New;
        centipedesList.Add(centipede);
    }

    //Добавление дополнительных препятствий. В данный момент реализованно перемещение объекта по одной из случайно выбранных
    //вертикальных линий внутри массива препятствий. В будущем имеет смысл добавить проверку взможности ращмещения новых препятствий
    //и размещение их на сцене
    private void AddBarriers()
    {
        if (Time.time > nextBomber)
        {
            nextBomber = Time.time + bomberRate;
            bomber.SetActive(true);
            float position = Random.Range(0, arrayOfBarriers.GetLength(0));
            bomber.transform.position = new Vector3(arrayStartPosition.x + position/2, 6, 0);
        }
    }   
}