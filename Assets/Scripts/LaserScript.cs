﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour {

    public float speed = 15f;
    float laserDestroyPos;
	
	void Start () {
        laserDestroyPos = MainSceneScript.arrayStartPosition.y - 0.5f;
	}
	
	void Update () {
        //Движение объекта
        Vector3 position = transform.position;

        position.y += speed * Time.deltaTime;

        transform.position = position;

        //Отключение объекта за пределами экрана
        if (!(this.transform.position.y > -5 && this.transform.position.y < laserDestroyPos))
        {
            gameObject.SetActive(false);           
        }


    }
}
