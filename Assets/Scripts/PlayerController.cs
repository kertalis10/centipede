﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float speed = 3f;
    private float fireRate = 0.35f;
    private float nextFire;
    public static int score;
    public static int life;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
        Vector3 position = transform.position;

        //Создание выстрела
        if (Input.GetKey("space") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            MainSceneScript.shotObjectPool.GetObject(new Vector3(position.x, position.y + 0.5f, 0));
        }

        //Управление 
        position.y += Input.GetAxis("Vertical") * speed * Time.deltaTime;
        position.x += Input.GetAxis("Horizontal") * speed * Time.deltaTime;

        //Ограничение возможной позиции игрока
        position = new Vector3
        (
            Mathf.Clamp(position.x, MainSceneScript.Left + 2, MainSceneScript.Right - 2),          
            Mathf.Clamp(position.y, MainSceneScript.Down + 0.5f, 0)
        );        

        transform.position = position;
    }

    void Death()
    {
        life--;
    }    
}
