﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Отвечает за вызов меню, и нажатия копок рестарт ми выход
public class PauseMenuScript : MonoBehaviour {

    public Transform canvas;

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }

        if (PlayerController.life <= 0)
        {
            EndGame();
        }

    }

    void Pause()
    {
        if (canvas.gameObject.activeInHierarchy == false)
        {
            canvas.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            canvas.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }

    void EndGame()
    {
        canvas.gameObject.SetActive(true);
        Time.timeScale = 0;
    }

    public void Quit()
    {        
        Application.Quit();
    }   

    public void Restart()
    {       
        SceneManager.LoadScene("main");
        Time.timeScale = 1;
    }
}
