﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierScript : MonoBehaviour {

    int health;
    public event System.Action Col = delegate { };

    void Start()
    {
        health = 3;
    }   

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bolt")
        {
            health--;
            other.gameObject.SetActive(false);
            if (health == 0)
            {
                gameObject.SetActive(false);
                health = 3;
                PlayerController.score += 100;
                Col = null;
                Change();
            }
        }
        else
        {
            Col.Invoke();
        }
    }

    void Change()
    {
        int i = (int)((MainSceneScript.arrayStartPosition.x - transform.position.x) * 2);
        i = i > 0 ? i : -i;
        int j = (int)((MainSceneScript.arrayStartPosition.y - transform.position.y) * 2);

        MainSceneScript.arrayOfBarriers[i, j] = 0;
    }
}
