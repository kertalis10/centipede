﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CentipedeScript : MonoBehaviour {

    public class Centipede
    {
        //Части противник
        List<GameObject> centipedeParts;
        //Координаты к которым будет двигатся объект на протяжении следующей итерации
        List<Vector3> coordinates;
        //Направление движения
        List<char> directions;
        //Скорость движения
        int speed;        

        public event System.Action Lose = delegate { };
        public event System.Action<Centipede> New = delegate { }, LastHit = delegate { };
        //Создание противника с заданной длинной и скоростью
        public Centipede(int length, int s)
        {
            centipedeParts = new List<GameObject>();
            coordinates = new List<Vector3>();
            directions = new List<char>();
            speed = s;

            GameObject newGameObject;
            newGameObject = Instantiate(Resources.Load<GameObject>("Prefabs/Head"));
            newGameObject.transform.position = new Vector3(0, MainSceneScript.Up, 0);
            centipedeParts.Add(newGameObject);
            coordinates.Add(new Vector3(0, MainSceneScript.Up - 0.5f, 0));
            directions.Add('d');
            centipedeParts[0].GetComponent<PartsOfCentScript>().Hit += Hit;
            centipedeParts[0].GetComponent<PartsOfCentScript>().number = 0;

            for (int i = 1; i < length; i++)
            {
                newGameObject = Instantiate(Resources.Load<GameObject>("Prefabs/BodyPart"));
                newGameObject.transform.position = new Vector3(0, MainSceneScript.Up + i * 0.5f, 0);
                centipedeParts.Add(newGameObject);
                coordinates.Add(new Vector3(0, MainSceneScript.Up + i * 0.5f - 0.5f, 0));
                directions.Add('d');
                centipedeParts[i].GetComponent<PartsOfCentScript>().Hit += Hit;
                centipedeParts[i].GetComponent<PartsOfCentScript>().number = i;

            }
        }
        //Деление противника
        public Centipede(Centipede c)
        {
            centipedeParts = new List<GameObject>();
            coordinates = new List<Vector3>();
            directions = new List<char>();
            speed = c.speed;

            int num = (int)Mathf.Ceil((float)(c.centipedeParts.Count / 2));

            GameObject newGameObject;
            newGameObject = Instantiate(Resources.Load<GameObject>("Prefabs/Head"));
            newGameObject.transform.position = c.centipedeParts[c.centipedeParts.Count - 1].transform.position;
            centipedeParts.Add(newGameObject);
            directions.Add(ChangeDirection(c.directions[c.centipedeParts.Count - 1]));
            coordinates.Add(ChangeCoordinates(directions[0], c.coordinates[c.centipedeParts.Count - 1]));
            centipedeParts[0].GetComponent<PartsOfCentScript>().Hit += Hit;
            centipedeParts[0].GetComponent<PartsOfCentScript>().number = 0;

            int count = 1;
           
            for (int i = c.centipedeParts.Count - 2; i > num; i--)
            {
                newGameObject = Instantiate(Resources.Load<GameObject>("Prefabs/BodyPart"));
                newGameObject.transform.position = c.centipedeParts[i].transform.position;
                newGameObject.GetComponent<PartsOfCentScript>().Hit += Hit;
                newGameObject.GetComponent<PartsOfCentScript>().number = count;
                centipedeParts.Add(newGameObject);
                directions.Add(ChangeDirection(c.directions[i]));
                coordinates.Add(ChangeCoordinates(directions[count], c.coordinates[i]));
                count++;
            }           
        }
        //изменения направления движения при делении
        private char ChangeDirection(char c)
        {
            if (c == 'd') return 'u';
            if (c == 'l') return 'r';
            if (c == 'r') return 'l';
            return 'd';
        }
        //изменение координат движения при делении
        private Vector3 ChangeCoordinates(char c, Vector3 v)
        {
            if (c == 'u') return new Vector3(v.x, v.y + 0.5f, 0);
            if (c == 'l') return new Vector3(v.x - 0.5f, v.y, 0);
            if (c == 'r') return new Vector3(v.x + 0.5f, v.y, 0);
            return new Vector3(v.x, v.y - 0.5f, 0);
        }

        public void Move()
        {           
            {
                //Если доходит до низа экрана, поражение
                if (centipedeParts[0].transform.position.y < -4)
                {
                    DestoyInstance();
                    Lose();
                }

                float fieldLeftSideLimit = MainSceneScript.arrayStartPosition.x;
                float fieldRightSideLimit = MainSceneScript.arrayStartPosition.x + MainSceneScript.arrayOfBarriers.GetLength(0) / 2;               

                //Логика движения головной части
                Vector3 position = centipedeParts[0].transform.position;

                if (directions[0] == 'u')
                {
                    centipedeParts[0].transform.rotation = Quaternion.Euler(0, 0, 90);
                    if (centipedeParts[0].transform.position.y < coordinates[0].y)
                    {
                        position.y += speed * Time.deltaTime;
                    }
                    else
                    {
                        //Переопределение направления движения и координат остальный частей, на базе предидущей части
                        for (int i = centipedeParts.Count - 1; i >= 1; i--)
                        {
                            directions[i] = directions[i - 1];
                            coordinates[i] = coordinates[i - 1];
                        }
                        directions[0] = 'l';
                        coordinates[0] = new Vector3(coordinates[0].x - 0.5f, coordinates[0].y, 0);
                    }
                }

                if (directions[0] == 'd')
                {
                    centipedeParts[0].transform.rotation = Quaternion.Euler(0, 0, -90);
                    if (centipedeParts[0].transform.position.y > coordinates[0].y)
                    {
                        position.y -= speed * Time.deltaTime;
                    }
                    else
                    {
                        //Переопределение направления движения и координат остальный частей, на базе предидущей части
                        for (int i = centipedeParts.Count - 1; i >= 1; i--)
                        {
                            directions[i] = directions[i - 1];
                            coordinates[i] = coordinates[i - 1];
                        }

                        //Выбор направления дальнейшего движения головы
                        if (centipedeParts[0].transform.position.x >= fieldRightSideLimit)
                        {
                            if (!IsBarrier(new Vector3(coordinates[0].x - 0.5f, coordinates[0].y, 0)))
                            {
                                directions[0] = 'l';
                                coordinates[0] = new Vector3(coordinates[0].x - 0.5f, coordinates[0].y, 0);
                            }
                            else
                            {
                                directions[0] = 'd';
                                coordinates[0] = new Vector3(coordinates[0].x, coordinates[0].y - 0.5f, 0);
                            }
                        }
                        else if (centipedeParts[0].transform.position.x <= fieldLeftSideLimit)
                        {
                            if (!IsBarrier(new Vector3(coordinates[0].x + 0.5f, coordinates[0].y, 0)))
                            {
                                directions[0] = 'r';
                                coordinates[0] = new Vector3(coordinates[0].x + 0.5f, coordinates[0].y, 0);
                            }
                            else
                            {
                                directions[0] = 'd';
                                coordinates[0] = new Vector3(coordinates[0].x, coordinates[0].y - 0.5f, 0);
                            }
                        }
                        else if (!IsBarrier(new Vector3(coordinates[0].x - 0.5f, coordinates[0].y, 0)))
                        {
                            directions[0] = 'l';
                            coordinates[0] = new Vector3(coordinates[0].x - 0.5f, coordinates[0].y, 0);
                        }
                        else if (!IsBarrier(new Vector3(coordinates[0].x + 0.5f, coordinates[0].y, 0)))
                        {
                            directions[0] = 'r';
                            coordinates[0] = new Vector3(coordinates[0].x + 0.5f, coordinates[0].y, 0);
                        }
                        else
                        {
                            directions[0] = 'd';
                            coordinates[0] = new Vector3(coordinates[0].x, coordinates[0].y - 0.5f, 0);
                        }
                    }
                }

                if (directions[0] == 'r')
                {
                    centipedeParts[0].transform.rotation = Quaternion.Euler(0, 0, 0);
                    if (centipedeParts[0].transform.position.x < coordinates[0].x)
                    {
                        position.x += speed * Time.deltaTime;
                    }
                    else
                    {
                        for (int i = centipedeParts.Count - 1; i >= 1; i--)
                        {
                            directions[i] = directions[i - 1];
                            coordinates[i] = coordinates[i - 1];
                        }

                        //Выбор направления дальнейшего движения головы
                        if (centipedeParts[0].transform.position.x >= fieldRightSideLimit)
                        {
                            directions[0] = 'd';
                            coordinates[0] = new Vector3(coordinates[0].x, coordinates[0].y - 0.5f, 0);
                        }
                        else if (IsBarrier(new Vector3(coordinates[0].x + 0.5f, coordinates[0].y, 0)))
                        {
                            directions[0] = 'd';
                            coordinates[0] = new Vector3(coordinates[0].x, coordinates[0].y - 0.5f, 0);
                        }
                        else
                        {
                            directions[0] = 'r';
                            coordinates[0] = new Vector3(coordinates[0].x + 0.5f, coordinates[0].y, 0);
                        }
                    }
                }

                if (directions[0] == 'l')
                {
                    centipedeParts[0].transform.rotation = Quaternion.Euler(0, 0, -180);
                    if (centipedeParts[0].transform.position.x > coordinates[0].x)
                    {
                        position.x -= speed * Time.deltaTime;
                    }
                    else
                    {
                        for (int i = centipedeParts.Count - 1; i >= 1; i--)
                        {
                            directions[i] = directions[i - 1];
                            coordinates[i] = coordinates[i - 1];
                        }

                        if (centipedeParts[0].transform.position.x <= fieldLeftSideLimit ||
                            IsBarrier(new Vector3(coordinates[0].x - 0.5f, coordinates[0].y, 0)))
                        {
                            directions[0] = 'd';
                            coordinates[0] = new Vector3(coordinates[0].x, coordinates[0].y - 0.5f, 0);
                        }
                        else
                        {
                            directions[0] = 'l';
                            coordinates[0] = new Vector3(coordinates[0].x - 0.5f, coordinates[0].y, 0);
                        }

                    }
                }

                centipedeParts[0].transform.position = position;

                //Логика движение остальных частей тела
                for (int i = 1; i < centipedeParts.Count; i++)
                {
                    position = centipedeParts[i].transform.position;
                    if (directions[i] == 'd')
                    {
                        centipedeParts[i].transform.rotation = Quaternion.Euler(0, 0, -90);
                        if (centipedeParts[i].transform.position.y > coordinates[i].y)
                        {
                            position.y -= speed * Time.deltaTime;
                        }
                    }
                    if (directions[i] == 'l')
                    {
                        centipedeParts[i].transform.rotation = Quaternion.Euler(0, 0, -180);
                        if (centipedeParts[i].transform.position.x > coordinates[i].x)
                        {
                            position.x -= speed * Time.deltaTime;
                        }
                    }
                    if (directions[i] == 'r')
                    {
                        centipedeParts[i].transform.rotation = Quaternion.Euler(0, 0, 0);
                        if (centipedeParts[i].transform.position.x < coordinates[i].x)
                        {
                            position.x += speed * Time.deltaTime;
                        }
                    }
                    centipedeParts[i].transform.position = position;
                }
            }
        }

        public void DestoyInstance()
        {
            foreach (var v in centipedeParts)
                Destroy(v);
        }

        //Определение есть ли по переданным координатам преграда
        private bool IsBarrier(Vector3 v)
        {
            int i = (int)((MainSceneScript.arrayStartPosition.x - v.x) * 2);
            i = i > 0 ? i : -i;
            int j = (int)((MainSceneScript.arrayStartPosition.y - v.y) * 2);

            try
            {
                if (MainSceneScript.arrayOfBarriers[i, j] == 1)
                {
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        private void Hit(int num)
        {
            if (centipedeParts.Count > 2 && Mathf.Ceil((float)centipedeParts.Count / 2) == num)
            {
                New(this);

                int count = centipedeParts.Count;

                for (int i = count - 1; i >= num; i--)
                {
                    Destroy(centipedeParts[i]);
                    centipedeParts.Remove(centipedeParts[i]);
                }  
            }
            else if (centipedeParts.Count == 1)
            {
                Destroy(centipedeParts.Last());
                centipedeParts.Remove(centipedeParts.Last());
                LastHit(this);
            }
            else
            {              
                Destroy(centipedeParts.Last());
                centipedeParts.Remove(centipedeParts.Last());
            }
        }
    }
}
