﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BomberScript : MonoBehaviour {

    public float speed = 15f;
    float destroyPos;
    public event System.Action Col = delegate { };

    
    void Start () {
        destroyPos = MainSceneScript.Down - 0.5f;
    }
	
	
	void Update () {
        Vector3 position = transform.position;

        position.y += -speed * Time.deltaTime;

        transform.position = position;

        //Отключение объекта за пределами экрана
        if (!(this.transform.position.y > destroyPos))
        {
            gameObject.SetActive(false);
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bolt")
        {
            gameObject.SetActive(false);
            PlayerController.score += 500;
        }
        else
        {
            Col.Invoke();
        }
    }
}
